const serviceList = [
  {
    img: {
      src: "/images/services-images/TREK.png",
      alt: "Trekking in Nepal",
    },
    title: "Trekking in Nepal",
    description:
      "Trekking in Nepal is the best option for travelers to spend their long and short holidays. Nepal Trek offers many trekking routes in the Himalayas regarding traveler's time frame and choices. Trek in Nepal, combining a city tour and other adventure sport makes your vacation wonderful with unforgettable experiences. Nepal Trek Adventure is one of the leading trekking company in Nepal. This agency includes the best itinerary in this region like Everest, Annapurna, all the eight-thousanders base camp trekking, Langtang, Makalu, Helambu, Manaslu, Eastern-western region trekking and more long-short treks and tours in Nepal. And also offers restricted areas, Upper Dolpo, Upper Mustang trekking and Rara Lake, Shey Phoksundo lake tours in Nepal. For making your trip adventurous, we have been serving all of our visitors and guests with an appropriate itinerary, facilities and reasonable cost with full of fun and authentic guest friendliness.All around the world, Nepal is one of the top destinations for everyone, mostly the mountaineers, trekkers etc. Welcome to the Beautiful country Nepal, land of the highest peak, Mt. Everest (Sagarmatha). Everyone's dream is to conquer the eight-thousanders peak, Out of the 14 highest peaks in the world, 8 of them are located in Nepal. This made tourists, mountaineers, trekkers to admit their dream to complete it by traveling successfully. Let your dream transform into reality.Journey in Nepal is almost incomplete for every tourist, trekkers, mountaineers who are backpacking without experiencing trekking from the land of mountains. Despite its geography, Nepal encapsulates vast cultural diversity, traditions, ethnic groups, multi-linguistic society, religions and other diversified social traits which pave the way to this country for researchers to scholars and thinkers to explorer.Apart from trekking and tours, the medley ground of this country offers varieties of adventure sports and outdoor activities that can get the adrenaline flowing. Peak Jungle Safari, Bungee Jumping, White Water Rafting, Mountain Biking, Cycling, Zip Flyer, Hot Air Ballooning, Sightseeing, Paragliding, Homestay Tour, Honeymoon Tour are among highlighted activities in Nepal.",
  },
  {
    img: {
      src: "/images/services-images/TOUR.jpg",
      alt: "Tour in Nepal",
    },
    title: "Tour in Nepal",
    description:
      "Nepal Trek Adventure is one of the best tour packages provider group tours leading travel agency in Nepal. Tour in Nepal package includes the most popular sightseeing destinations like Kathmandu valley, Pokhara city, Chitwan jungle safari, Lumbini- Buddha's birthplace visit, the pilgrimage tour and more. We have customized some Nepal tour package which can fulfill your dream of the visit. The package makes your tour plan comfortable and easy. Besides that, we also provide you a chance to learn the culture, tradition, unique cultural heritage sites and glorious history of Nepal.Nepal is a peculiar tourist's paradise in every sense of pleasure. Himalayan ranges, river, religion, wildlife sanctuaries, monuments, Temples, museums, and cultures are the world heritage sites of this country. It is a land of peace where Lord Buddha was born. It is blessed by the divine power of natural beauty. It is a major tourist spot in Asia. Because of the unbeatable combination of natural beauty, it is one of the best destinations for the tour in Nepal as well as Trekking, Jungle Safari, rafting and many more adventure sports. Experience of Nepal not only limits within its natural beauty.Nepal is one of the best countries for sightseeing tours, with its long history, fascinating art and distinctive and varied architecture. The vast diversity of people, their culture, language, and traditions will attract anyone. The two predominant religions, Hinduism and Buddhism, are ever present with the variety of gods and goddess, numerous temples, stupas, and monasteries, depict the deep faith of the people. The people of the country are more beautiful and simple. Their different cultures, festivals, religion, and tradition fascinate you equally. Nepal Trek Adventures warmly welcomes all travelers who want to visit Nepal. Our pre-designed packages offer you a complete trip package as per your demand.Hence; within an hour of flight, you reach the peak of the world and the lowest point of the country. Probably, it is the only country in the world that was not colonized by any other country. The bravery of the country speaks aloud from India to England. Nepal Trek Adventure (NTA) is one of the leading tour operators in Nepal offering some exciting specific tours in the country. With official motto-Satisfied Customer Our Assets- we give you a great experience of the journey making it memorable throughout your life. The specific programmers give visitors an opportunity to observe the rich Nepalese.",
  },
  {
    img: {
      src: "/images/services-images/ADV.jpg",
      alt: "Adventure Acitivities",
    },
    title: "Adventure Acitivities",
    description:
      "Adventure activities in Nepal are the milestone for adventure lovers. These activities bring out the child that you ought to have esoteric of you. These allow you to feel nature and lose oneself to the surrounding. Nepal is the home for the adventure lovers as anyone can enjoy trekking, tours, and adventure activities here. Besides Everest, Annapurna and more trekking sites and also beyond Pashupatinath, Swoyambhunath, and many religious sites; there resides the spots where you can try new kinds of stuff named as adventure activities. Nepal encircles a lot of them, making you able to get in touch with the nature of Nepal more proficiently.Nepal, the Himalayan country of the world, delivers the outstanding outdoor adventures like trekking, rock climbing, canyoning, bungee jumping, zip flyer, rafting, mountain flight, mountain biking, rock climbing, hot air ballooning, and so forth. All these particular attractions attract thousands of tourists yearly towards the destination spots. Nepal and Nepalese adventures teach that the only way of enjoying the nature of the beautiful nation is to have an adventure and get thrilled by its wonderful feelings. Trekking has been the extraordinary result of Nepal since a long is as yet favored by gutsy one, originating from the various parts of the world. Apart from that, other adventure activities are gaining more and more popularity day by day.",
  },
  {
    img: {
      src: "/images/services-images/HIKE.jpg",
      alt: "Hiking in Nepal",
    },
    title: "Hiking in Nepal",
    description:
      "Hiking is likely to walk or march a great distance, especially through rural areas, for pleasure and likely to know the lifestyles of local people of that region. Hiking in Nepal is one of the best adventure experiences as the country has ample of hiking trails. One can enjoy tough hiking to moderate or easiest. For a challenging hike, Mount Everest and Annapurna region are the best options whereas for short and easiest trail Nagarkot Day Hike around Kathmandu, Helambu Hike, and Ghorepani Poon Hill are preferred.Nepal Trek Adventures is the one of the best traveling agency. Nepal, a country of highest peaks and Natural beauty, is no. 1 travel destinations in the world for outdoor activities such as Hiking, Mountaineering and peak climbing, Trekking, White water rafting, Bungee jumping, paragliding, sightseeing in Nepal. These adventurous activities provide various adventure experiences. Among these, Hiking is one of the best sources of touring the area and Natural exercise for physical and mental fitness. The most important thing to remember in Hiking is 'The journey is more important than the destination'.Actually, as the country is full of green lush valley most of them are beautiful and attracting. Still, we have chosen some best and pretty from other many not much far from Kathmandu valley are as follows.",
  },
  {
    img: {
      src: "/images/services-images/HELI.jpeg",
      alt: "Heli Tour in Nepal",
    },
    title: "Heli Tour in Nepal",
    description:
      "Helicopter tour in Nepal, the greatest way of exploring the beautiful Himalayan country, Nepal, is right on your side. It is the best opportunity to enjoy Nepalese beauty as a falcon. Nepal Helicopter Tour is a great adventure filled with an awesome and aspiring country scenario with the Himalayas. It is especially suitable for time-bounded travelers who want to enjoy the journey in a short amount of time. Also, this helicopter ride is organized for people whose physical conditions do not support their mental will.Helicopter Ride in Nepal can be your ideal choice as it provides the dream comes a true moment for those who fantasize to explore different places with different attractions but are not able to do so in a precise period of time. In this Himalayan country, you can have different scenic flights for exploring beautiful mountain panorama, Himalayan glaciers, national parks, city areas, lakesides, green hills, and many more mysterious and remote destinations with the help of these helicopters in Nepal.Other choices can be made for Wedding via Helicopter Tour, Helicopter Tour Kathmandu, Helicopter Tour Pokhara, birthday celebration on the Himalayas via Heli Tour, stepping on base camps by helicopter, Mountain helicopter tour, Pilgrimage Area Helicopter Tour, Helicopter evacuation, Emergency helicopter Support, and many more. Flyover Kathmandu valley, Flyover Pokhara valley, Flyover Chitwan valley, Flyover Barun valley, Flyover inner Terai valley, Flyover Marshyangdi valley can also be done as the part of flyover 2020 packages in Nepal. Are you planning to step on Nepal for the chopper ride in Nepal? If yes, you may be curious to know about different criteria like packages, costs, and other important information. Here are some.",
  },
];

const whyCompanyList = [
  {
    src: "/images/COMPANY-LOGO.png",
    alt: "medical.svg",
    title: "main reason why",
    description:
      "Nepal Everest Holidays is a specialized company for indoor and outdoor adventurous journey in Nepal. It is a local company run by trekking experts with years of knowledge about the Nepalese tourism sector. With our step in the tourism sector, we have to offer excess information about tourism in Nepal including facts and views about different places and available tours, trekking, and adventure sports in the respective area. We also aid local guides and porters in order to increase their income and maintain overall nations’ economy. We always focus on best sellers in Nepal and offer best and reasonable prices; packages that you can fully enjoy. Those prices are negotiable too. Uniting with us will be beneficial for you as we provide any kinds of the voyage in Nepal with best reasonable prices and we do not resell the trip or use third-party association for making the trip successful.",
  },
  {
    src: "/images/COMPANY-LOGO.png",
    alt: "medical.svg",
    title: "Authorization and Experience",
    description:
      "Nepal Everest Holidays is a registered company which is authorized to perform the adventurous activities like trekking, climbing, expedition, hiking, tours, and sight-seeing, safari and many more tourism-related activities like paragliding, zip flyer, hot air ballooning, mountain flights, bungee jumping, rafting in Nepal. Beyond Nepal, it offers you the trip to India and Tibet. We have long-term experience in national and international tourism field. We have never broken the rules and regulations mentioned by Nepal Tourism Board and is walking in our own way in this industry.",
  },
  {
    src: "/images/COMPANY-LOGO.png",
    alt: "medical.svg",
    title: "Local Experts and Equipment",
    description:
      "Nepal Everest Holidays has 50 members in the company including guides, porters, managers, and other personnel. All of these people are locally inherited and are expert in their own field of industry. We offer local guides and porters who have well knowledge about places, language, and tourism-related facts. They will assist you a lot in your trek and give their best to make your trip successful and memorial. You can buy journey equipment for yourself or ask us to buy for you. We use local outfitters in Nepal for this purpose.",
  },
  {
    src: "/images/COMPANY-LOGO.png",
    alt: "medical.svg",
    title: "Varieties of Services",
    description:
      "Nepal Everest Holidays provides various kinds of the voyage in Nepal such as trekking, sightseeing, safaris, peak climbing, caving, cultural tours, village tour, sports, and many more. We always ensure that our each and every trip becomes entertaining, interesting and enthusiastic. We also run our trip in Tibet and India. Services of food, lodges/hotels, permits, accommodation, charter arrangements are also provided by us.",
  },
];

export { serviceList, whyCompanyList };
