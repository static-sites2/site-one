import Head from "next/head";
import styles from "../styles/Home.module.css";
import { useEffect } from "react";
import { Row, Col } from "antd";

export default function Home() {
  useEffect(() => {
    setTimeout(() => {
      const cvr = document.getElementById("coverWrapper");
      cvr.style.height = "350px";
      cvr.style.padding = "50px 0";
    }, 100);
  });
  return (
    <div className={styles.container}>
      <Head>
        <title>Nepal Everest Holidays</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.main}>
        <div className={styles.coverWrapper} id="coverWrapper">
          <Row>
            <Col span={12} className={styles.cwLeft}>
              <img src="/images/COMPANY-LOGO.png" alt="company-logo.png" />
            </Col>
            <Col span={12} className={styles.cwRight}>
              <h1>Nepal Everest Holidays</h1>
              <h3>One of Nepal's best travel company.</h3>
              <p>Your travel partner all around Nepal.</p>
            </Col>
          </Row>
        </div>
        <div className={styles.homeContent} id="homeAboutUs">
          <h1>About Us</h1>
          <p>
            Nepal Everest Holidays is one of the leading trekking company of
            Nepal. It is a government registered travel agency which is
            recognized by Trekking Agencies' Association of Nepal. It is a
            client-oriented organization which was established in 2006 with a
            sole objective “Satisfied Customer Our Assets”. The organization has
            been providing the wide range of trek and tour services to thousands
            of tourists since then. The reviews of our customers’ speak aloud
            about us keeping NEH different from the crowd of trekking agencies.
          </p>
        </div>
        <div className={styles.homeContent}>
          <h1>Our Experiences</h1>
          <p>
            Nepal Everest Holidays has a set of well experienced, self-motivated
            and dedicated team who work around a clock to make your holiday
            package memorable, cherishable and safe. The organization has more
            than 20 certified trekking guides and around 10 tour guides. NEH
            provides local trekking guides / Sherpa to the customers so that
            they (customers) are well informed about the social, geographical,
            historical, and economic condition of the trekking routes or areas.
            The organization under Corporate Social Responsibility (CSR) has
            stepped-up with the concept of the local guide. Most of the guides
            are English speaking. But, to meet your convenience and short out
            language problem, NEH also provides guides who can speak and
            communicate with you in your native language.
          </p>
        </div>
        <div className={styles.homeContent}>
          <h1>Our Services</h1>
          <p>
            Nepal Everest Holidays is also famous for arranging hiking in Nepal,
            planning Nepal holidays, helicopter tours, Nepal honeymoon tour,
            Everest Base Camp Luxury Lodge Trek and other many more long and
            short trekking in Nepal as per your request. Moreover, expedition in
            Nepal is not only limited within trekking and tour. In the past few
            years, Nepal has established itself as a destination for adventurous
            activities (sports). Tourist can enjoy rafting, zip flyer,
            paragliding, kayaking, canoeing, canyoning and many more.
          </p>
        </div>
      </div>
    </div>
  );
}
