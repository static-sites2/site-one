import "antd/dist/antd.css";
import "../styles/globals.css";
import HeaderBar from "../components/Header";
import Footer from "../components/Footer";
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta charset="utf-8" />
        <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />

        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
        />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script
          defer
          src="https://use.fontawesome.com/releases/v5.12.0/js/all.js"
        ></script>
        <script
          defer
          src="https://use.fontawesome.com/releases/v5.12.0/js/v4-shims.js"
        ></script>
      </Head>
      <HeaderBar />
      <Component {...pageProps} />
      <Footer />
    </>
  );
}

export default MyApp;
