import { Row, Col } from "antd";
import { whyCompanyList } from "../utilities/mockdata";
import styles from "./../styles/InfoRowCol.module.css";

export default function Product() {
  return (
    <div className={styles.container}>
      <h1 className={`${styles.contentTitle} ${styles.contentTitleGreen}`}>Why Nepal Everest</h1>
      <div className={styles.rowColWrapper}>
        {whyCompanyList.map((wc, i) => (
          <Row
            className={`${styles.rowColRow} ${
              i % 2 === 1 && styles.rowInverse
            }`}
            key={i}
          >
            <Col
              span={12}
              className={`${styles.rowColCol} ${
                styles[i % 2 === 0 ? "colLeft" : "colRight"]
              }`}
            >
              <img src={wc.src} alt={wc.alt} />
            </Col>
            <Col
              span={12}
              className={`${styles.rowColCol} ${
                styles[i % 2 === 0 ? "colRight" : "colLeft"]
              }`}
            >
              <h3 className='textGreen'>{wc.title}</h3>
              <p>{wc.description}</p>
            </Col>
          </Row>
        ))}
      </div>
    </div>
  );
}
