import { Col, Image, Row } from "antd";
import { numberOfImages } from "../utilities/constants";
import styles from "../styles/InfoRowCol.module.css";

export default function Gallery() {
  let galleryImages = [];
  const swapCount = Math.round(numberOfImages / 4);
  for (let index = 1; index <= numberOfImages; index++) {
    galleryImages.push({
      src: `/images/gallery-images/gallery-image${index}.jpeg`,
      alt: index % 2 === 0 ? "Trekking in Nepal" : "Tour in Nepal",
    });
  }

  return (
    <div className={styles.container}>
      <h1 className={`${styles.contentTitle} ${styles.contentTitleBlue}`}>
        Gallery
      </h1>
      <Row className={styles.galleryImageRow}>
        <Col span={6} className={styles.galleryImageColumn}>
          {galleryImages.slice(0, swapCount).map((gi, i) => (
            <Image
              className={styles.galleryImage}
              width={"100%"}
              src={gi.src}
              key={i}
              alt={gi.alt}
              fallback="/images/COMPANY-LOGO.png"
            />
          ))}
        </Col>
        <Col span={6} className={styles.galleryImageColumn}>
          {galleryImages.slice(swapCount, swapCount * 2).map((gi, i) => (
            <Image
              className={styles.galleryImage}
              width={"100%"}
              src={gi.src}
              key={i}
              alt={gi.alt}
              fallback="/images/COMPANY-LOGO.png"
            />
          ))}
        </Col>
        <Col span={6} className={styles.galleryImageColumn}>
          {galleryImages.slice(swapCount * 2, swapCount * 3).map((gi, i) => (
            <Image
              className={styles.galleryImage}
              width={"100%"}
              src={gi.src}
              key={i}
              alt={gi.alt}
              fallback="/images/COMPANY-LOGO.png"
            />
          ))}
        </Col>
        <Col span={6} className={styles.galleryImageColumn}>
          {galleryImages.slice(swapCount * 3, swapCount * 4).map((gi, i) => (
            <Image
              className={styles.galleryImage}
              width={"100%"}
              src={gi.src}
              key={i}
              alt={gi.alt}
              fallback="/images/COMPANY-LOGO.png"
            />
          ))}
        </Col>
      </Row>
    </div>
  );
}
