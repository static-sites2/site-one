import { Row, Col } from "antd";
import { serviceList } from "../utilities/mockdata";
import styles from "../styles/InfoRowCol.module.css";

export default function Services() {
  return (
    <div className={styles.container}>
      <h1 className={`${styles.contentTitle} ${styles.contentTitleYellow}` }>Our Services</h1>
      <div className={styles.rowColWrapper}>
        {serviceList.map((sl, i) => (
          <Row
            className={`${styles.rowColRow} ${
              i % 2 === 1 && styles.rowInverse
            }`}
            key={i}
          >
            <Col
              span={12}
              className={`${styles.rowColCol} ${
                styles[i % 2 === 0 ? "colLeft" : "colRight"]
              }`}
            >
              <img src={sl.img.src} alt={sl.img.alt} />
            </Col>
            <Col
              span={12}
              className={`${styles.rowColCol} ${
                styles[i % 2 === 0 ? "colRight" : "colLeft"]
              }`}
            >
              <h3 className="textYellow">{sl.title}</h3>
              <p>{sl.description}</p>
            </Col>
          </Row>
        ))}
      </div>
    </div>
  );
}
