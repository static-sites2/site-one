import Link from "next/link";
import styles from "./../../styles/Header.module.css";

export default function NavBar() {
  return (
    <div className={styles.headerNavWrapper}>
      <Link href="/">
        <a>Home</a>
      </Link>
      <Link href="/services">
        <a>Services</a>
      </Link>
      <Link href="/whyCompany">
        <a>Why Us</a>
      </Link>
      <Link href="/gallery">
        <a>Gallery</a>
      </Link>
    </div>
  );
}
