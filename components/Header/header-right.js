import styles from "../../styles/Header.module.css";
import { Button, message } from "antd";

export default function HeaderRight() {
  return (
    <div className={styles.headerRightWrapper}>
        <Button disabled type='primary' onClick={()=>message.warning('Under Development')}>Login</Button>
        <Button disabled type='secondary' onClick={()=>message.warning('Under Development')}>Sign up</Button>
    </div>
  );
}
