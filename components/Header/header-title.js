import Link from "next/link";
import styles from "../../styles/Header.module.css";

export default function HeaderTitle() {
  return (
    <div className={styles.headerTitleWrapper}>
      <Link href="/">
        <a>
          <img
            src={"/images/COMPANY-LOGO.png"}
            alt="companylogo"
            width="30px"
            height="30px"
          />
          <span>Nepal Everest Holidays</span>
        </a>
      </Link>
    </div>
  );
}
