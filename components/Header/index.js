import NavBar from "./header-navbar";
import HeaderTitle from './header-title';
import HeaderRight from './header-right';
import styles from './../../styles/Header.module.css';

export default function HeaderBar(){
    return(
        <div className={styles.headerWrapper}>
            <HeaderTitle/>
            <NavBar />
            <HeaderRight/>
        </div>
    )
}