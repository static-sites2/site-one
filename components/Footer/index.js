import { useState } from "react";
import { message, Input, Form, Button, Row, Col } from "antd";
import styles from "./../../styles/Footer.module.css";
import { emailJSConfig } from "../../utilities/constants";
import emailjs from "emailjs-com";

const initialFeedBackData = {
  fullName: "",
  emailId: "",
  phone: "",
  message: "",
};

export default function Footer() {
  const [subscribeMail, setSubMail] = useState("");
  const [feedBackData, setFeedBackData] = useState(initialFeedBackData);
  const [isSubmitting, setIsSubmitting] = useState(false);

  function handleChange(e) {
    const { name, value } = e.target;
    setFeedBackData((prevState) => ({ ...prevState, [name]: value }));
  }
  function handleSubscribed(e) {
    e.preventDefault();
    const emailData = {
      subscribe_mail: subscribeMail,
      to_email: emailJSConfig.TO_EMAIL,
      to_name: emailJSConfig.TO_NAME,
    };
    emailjs
      .send(
        emailJSConfig.SERVICE_ID,
        emailJSConfig.TEMPLATE_SUBSCRIBE,
        emailData,
        emailJSConfig.USER_ID
      )
      .then((response) => {
        message.success("Subscribed successfully.");
        setSubMail('');
      })
      .catch((error) => {
        console.trace("err: ", error);
        message.error("Subscribtion faliure.");
      });
  }
  function handleSubmit(e) {
    e.preventDefault();
    if (validation()) {
      setIsSubmitting(true);
      const emailData = {
        message_html: `${feedBackData.message} \n\n Email: ${feedBackData.emailId}`,
        from_phone: feedBackData.phone,
        from_name: feedBackData.fullName,
        reply_to: feedBackData.emailId,
        to_email: emailJSConfig.TO_EMAIL,
        to_name: emailJSConfig.TO_NAME,
      };
      emailjs
        .send(
          emailJSConfig.SERVICE_ID,
          emailJSConfig.TEMPLATE_CONTACT,
          emailData,
          emailJSConfig.USER_ID
        )
        .then((response) => {
          message.success("Enquiry sent successfully.");
          setFeedBackData(initialFeedBackData);
          setIsSubmitting(false);
        })
        .catch((error) => {
          console.trace("err: ", error);
          message.error("Enquiry sending error.");
          setIsSubmitting(false);
        });
    }
  }
  function validation() {
    let _errors = {};
    if (!feedBackData.emailId) _errors.emailId = "Email Address is required.";
    if (!feedBackData.message) _errors.message = "Message is required.";
    return Object.keys(_errors).length === 0;
  }
  return (
    <div className={styles.footerWrapper} id="footerWrapper">
      <div className={styles.subscribeWrapper}>
        <div className={styles.subscribeContent1}>
          Sign up for <b>newsletters :</b>
        </div>
        <form onSubmit={handleSubscribed} className={styles.subscribeForm}>
          <input
            type="text"
            name="emailId"
            placeholder="Email Address to Subscribe"
            value={subscribeMail}
            onChange={(e) => {
              setSubMail(e.target.value);
            }}
          />
          <Button
            type="primary"
            onClick={handleSubscribed}
            {...Object.assign({}, subscribeMail ? null : { disabled: true })}
          >
            Subscribe
          </Button>
        </form>
        <div className={styles.subscribeContent2}>
          Get news, notifications, discount, offers and updates about the recent
          events, offers and promotions from/of our company.
        </div>
      </div>
      <Row className={styles.contactUs}>
        <Col span={8} className={styles.contactForm}>
          {isSubmitting && (
            <div className={styles.ContactFormloader}>
              <div className={styles.loader} />
            </div>
          )}
          <Form>
            <h3>Send Enquiry</h3>
            <Input
              name="fullName"
              prefix={<i className="fas fa-user" />}
              placeholder="Full Name"
              className={styles.contactFormInput}
              value={feedBackData.fullName}
              onChange={handleChange}
            />
            <Input
              name="emailId"
              prefix={<i className="fas fa-envelope" />}
              placeholder="Email Address"
              className={styles.contactFormInput}
              value={feedBackData.emailId}
              onChange={handleChange}
            />
            <Input
              name="phone"
              prefix={<i className="fas fa-phone-alt" />}
              placeholder="98XXXXXXXX"
              className={styles.contactFormInput}
              value={feedBackData.phone}
              onChange={handleChange}
            />
            <Input.TextArea
              name="message"
              placeholder="Message..."
              className={styles.contactFormTextArea}
              style={{ height: "120px" }}
              value={feedBackData.message}
              onChange={handleChange}
            />
            <Button
              type="primary"
              htmlType="submit"
              onClick={handleSubmit}
              {...Object.assign({}, validation() ? null : { disabled: true })}
            >
              Submit
            </Button>
          </Form>
        </Col>
        <Col span={8} className={styles.contactInfo}>
          <h3>Contact Info</h3>
          <p>
            Phone: <b>+977-1-4428382 </b>
          </p>
          <p>
            Mobile: <b>+977-9851041736</b>
          </p>
          <p>
            Email: <b>nepaleverestholiday@gmail.com</b>
          </p>
          <div className={styles.contactInfoSocial}>
            <h6>Find us on:</h6>
            <a href="https://www.facebook.com">
              <i class="fab fa-facebook-square"></i>
            </a>
            <a href="https://www.instagram.com">
              <i class="fab fa-instagram"></i>
            </a>
            <a href="https://www.twitter.com">
              <i class="fab fa-twitter-square"></i>
            </a>
            <a href="https://www.linkedin.com">
              <i class="fab fa-linkedin"></i>
            </a>
          </div>
        </Col>
        <Col span={8} className={styles.contactInfoMapWrapper}>
          <h6>Find us on Google Map</h6>
          <iframe
            className={styles.contactInfoMap}
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d882.9903456464256!2d85.31133882923582!3d27.718478586128743!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19e9ca0355fb%3A0xb42822caa8a8f277!2sNepal%20Everest%20Holidays%20Pvt.%20Ltd!5e0!3m2!1sen!2snp!4v1620570651478!5m2!1sen!2snp"
            frameborder="0"
            allowfullscreen=""
            aria-hidden="false"
            tabindex="0"
          ></iframe>
        </Col>
      </Row>
      <div className={styles.copyright}>
        @Copyright to Nepal Everest Holidays Pvt. Ltd.
      </div>
    </div>
  );
}
